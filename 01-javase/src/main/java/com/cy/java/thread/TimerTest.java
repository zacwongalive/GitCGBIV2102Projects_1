package com.cy.java.thread;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 单线程任务调度(通过线程按照一定的周期去执行任务)
 */
public class TimerTest {
    public static void main(String[] args) {
        //构建timer对象(此对象构建时会初始化一个线程，还有一个任务队列)
        Timer timer=new Timer();
        timer.schedule(new TimerTask() {//任务对象
            @Override
            public void run() {
                String tname=Thread.currentThread().getName();
                System.out.println(tname+"->task1");
            }
        }, 2000);//5秒以后执行任务
        timer.schedule(new TimerTask() {//任务对象
            @Override
            public void run() {
                String tname=Thread.currentThread().getName();
                System.out.println(tname+"->task2");
            }
        }, 3000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                String tname=Thread.currentThread().getName();
                System.out.println(tname+":"+System.currentTimeMillis());
            }
        }, 3000, 1000);
    }
}
