package com.cy.java.thread;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledExecutorServiceTest {
    public static void main(String[] args) {
        ScheduledExecutorService ses=
        Executors.newScheduledThreadPool(2);
        ses.scheduleAtFixedRate(new Runnable() {
            public void run() {
                System.out.println(System.currentTimeMillis());
            }
        }, 1, 1, TimeUnit.SECONDS);
        ses.scheduleAtFixedRate(new Runnable() {
            public void run() {
                System.out.println(System.nanoTime());
            }
        }, 1, 1, TimeUnit.SECONDS);
    }
}
