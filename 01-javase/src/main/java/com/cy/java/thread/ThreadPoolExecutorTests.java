package com.cy.java.thread;
//J.U.C
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorTests {
    public static void main(String[] args) {
        //阻塞式队列对象(通过此队列存储排队等待执行的一些任务对象)
        BlockingQueue<Runnable> workQueue=
                new ArrayBlockingQueue<Runnable>(1);
        //阿里巴巴推荐的一种创建线程池的方式 (最灵活)
        ThreadPoolExecutor tPool=
                new ThreadPoolExecutor(2, //corePoolSize 核心线程数
                        3,//maximumPoolSize 最大线程数
                        60, //keepAliveTime 最大空闲时间
                        TimeUnit.SECONDS,//unit 时间单位
                        workQueue);
        //通过池中的线程执行任务
        tPool.execute(new Runnable() {
            public void run() {
                String tName=Thread.currentThread().getName();
                System.out.println(tName+" execute task01 ");
                try{Thread.sleep(5000);}catch(Exception e){}
            }
        });
        tPool.execute(new Runnable() {
            public void run() {
                String tName=Thread.currentThread().getName();
                System.out.println(tName+" execute task02 ");
                try{Thread.sleep(5000);}catch(Exception e){}
            }
        });
        tPool.execute(new Runnable() {
            public void run() {
                String tName=Thread.currentThread().getName();
                System.out.println(tName+" execute task03 ");
            }
        });
        tPool.execute(new Runnable() {
            public void run() {
                String tName=Thread.currentThread().getName();
                System.out.println(tName+" execute task04 ");
                try{Thread.sleep(5000);}catch(Exception e){}
            }
        });
        tPool.execute(new Runnable() {
            public void run() {
                String tName=Thread.currentThread().getName();
                System.out.println(tName+" execute task05 ");
            }
        });
    }
}
