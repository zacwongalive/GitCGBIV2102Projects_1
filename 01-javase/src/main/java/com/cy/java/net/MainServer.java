package com.cy.java.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Web 服务
 */
public class MainServer {//tomcat
    public static void main(String[] args) throws IOException {
        boolean flag=true;
        //启动服务
        ServerSocket server=new ServerSocket(9000);
        System.out.println("server start");
        //等待客户端连接
        while(flag){
           Socket socket=server.accept();
           System.out.println("客户端已连接："+socket);
        }
    }
}
