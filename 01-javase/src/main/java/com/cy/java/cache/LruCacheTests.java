package com.cy.java.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCacheTests {
    static LinkedHashMap<String,Integer> cache=
            new LinkedHashMap<String, Integer>(3,
                    0.75f,
                    true){//true表示记录访问顺序，默认记录添加顺序
        //每次执行put操作，此方法都会执行，true表示移除元素
        @Override
        protected boolean removeEldestEntry(
                Map.Entry<String, Integer> eldest) {
            //System.out.println("eldest="+eldest);
            return cache.size()>3;
        }
    };
    public static void main(String[] args) {
        cache.put("A", 100);
        cache.put("B", 200);
        cache.put("C", 300);
        cache.get("A");
        cache.put("D", 400);
        cache.put("E", 500);
        System.out.println(cache);//cache.toString()



    }
}
