package com.cy.java.basic;

public class IntegerCacheTests {
    public static void main(String[] args) {
        //Integer类内部存在一个整数池，池中默认存储了-128~+127
        Integer t1=100;//系统底层会自动封箱，默认调用Integer.valueOf(100)
        Integer t2=100;
        System.out.println(t1==t2);//true
        Integer t3=200;//Integer.valueOf(200)检测池中没有，则new Integer(100)
        Integer t4=200;
        System.out.println(t3==t4);//false
    }
}
