package com.cy.java.oop;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class Point{
    int x;
    int y;
    public Point(int x,int y){
        this.x=x;
        this.y=y;
    }
    //此方法用于监控对象是否要被销毁了，销毁之前会调用此方法
    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize()");
    }
}
//JVM参数
//输出GC信息：-XX:+PrintGC
public class ReferenceTests {
    public static void main(String[] args) {
        //1.p1为强引用(此引用引用对象，生命力最顽强，即便是内存溢出，对象不会给GC)
       // Point p1=new Point(10,20);
       // p1=null;//此时20行创建的对象就会变为垃圾对象
        //2.弱引用(可以系统触发GC时直接销毁引用的对象,生命力最弱的一种引用方式)
        WeakReference<Point> p2=
                new WeakReference<Point>(new Point(20,30));
        System.out.println(p2.get().x);
        //3.软引用(在系统内存不足时，此引用引用的对象可能会被销毁，生命力要比弱引用强一些)
        SoftReference<Point> p3=
                new SoftReference<Point>(new Point(30,50));
        //启动系统JVM的GC系统
        //手动启动GC
        //System.gc();
        //自动启动GC
        List<byte[]> list=new ArrayList<byte[]>();
        for(int i=0;i<10000;i++){
            list.add(new byte[1024*1024]);
        }
    }
}
