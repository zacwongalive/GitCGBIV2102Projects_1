package com.cy.com.cy.sca.notice.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 处理远程短信请求
 */
@RestController
@RequestMapping("/notice/")
public class NoticeController {

    @PostMapping("/sms/send")
    public String doSendCodeMsg(@RequestBody Map<String,Object> msg){
        System.out.println("notice.msg="+msg);
        //后续可以在此位置调用短信平台发短息
        return "send ok";
    }
    @PostMapping("/log/record")
    public String doLoginfo(@RequestBody Map<String,Object> log){
        System.out.println("notice.log="+log);//拿到了
        //后续可以在此位置调用短信平台发短息
        return "log ok";
    }
    //....
}
