package com.cy.sca.api.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**自定义注解(第一阶段)*/
@Retention(RetentionPolicy.RUNTIME)//定义我们自己写的注解何时有效
@Target(ElementType.METHOD)//定义我们写的注解可以描述的成员
public @interface RequiredLog {
}
