package com.cy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ScaGatewayApplication implements CommandLineRunner {

    @Value("${server.port}")
    private  String server;

    public static void main(String[] args) {
        SpringApplication.run(ScaGatewayApplication.class,args);
    }
    /**
     * Spring Boot 工程中，假如希望在服务启动后，获取一些配置信息，并将其
     * 输出，可以让启动类实现CommandLineRunner接口，并重写run方法，此方法
     * 会在服务启动以后自动执行
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        System.out.println("server="+server);
    }
}
