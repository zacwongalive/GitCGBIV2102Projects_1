package com.cy;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GatewayApplication {//WebFlux

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }
/*    @Bean
    public IRule rule(){
        return new NacosRule();
    }*/

}
