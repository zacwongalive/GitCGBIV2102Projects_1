package com.cy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 定义Feign接口，通过此接口中的方法实现远程过程调用？
 * 注意：此接口中只定义访问的url信息，不做具体访问实现（底层会在系统初始化时
 * 创建接口实现类-JDK代理，然后在代理类中做具体访问实现）
 *
 * @FeignClient注解描述的接口，为远程服务调用接口，注解中name属性的值为
 * 要远程调用的服务名
 */
//@FeignClient("nacos-provider")
@FeignClient(value="nacos-provider",contextId = "consumerHttpFeignAPI")
//@RestController 可以省略
public interface ConsumerHttpApi {
    /**这个方法表示要向/provider/echo/{msg}这个地址发起远程过程调用*/
    @GetMapping("/provider/echo/{msg}")
    public String echoMsg(@PathVariable String msg);
}

