package com.cy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@RestController
@RequestMapping("/consumer/template/")
public class RestConsumerController {
    /**通过此对象进行远端服务调用-RPC*/
    @Autowired
    private RestTemplate loadBalancedRestTemplate;

    //http://localhost:8090/consumer/template/15
    @DeleteMapping("{id}")
    public String doDeleteById(@PathVariable Integer id){
        String url="http://nacos-provider/provider/template/"+id;
        System.out.println("consumer.url="+url);
        //通过服务提供方法执行删除操作
        loadBalancedRestTemplate.delete(url);
        return "delete ok";
    }

    /**Post请求用于执行添加操作，当使用requestBody注解描述参数时，表示
     * 这个参数的值来自一个json格式的字符串，对于一个方法而言，只能有一个
     * 参数使用@RequestBody注解*/
    @PostMapping
    public Map<String,Object> doPost(@RequestBody  Map<String,Object> map){
        String url="http://nacos-provider/provider/template/";
        return loadBalancedRestTemplate.postForObject(url, map,Map.class );
    }
    /**Put请求方式一般用于更新操作*/
    @PutMapping
    public String doPut(@RequestBody Map<String,Object> map){
        String url="http://nacos-provider/provider/template/";
        loadBalancedRestTemplate.put(url, map);
        return "update ok";
    }


}
