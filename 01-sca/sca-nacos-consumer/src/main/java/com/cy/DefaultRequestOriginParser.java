package com.cy;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;
import javax.servlet.http.HttpServletRequest;

/**
 * 构建请求参数解析对象，基于此对象的返回值进行授权访问设计
 */
@Component
public class DefaultRequestOriginParser implements RequestOriginParser {

    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        //基于请求参数值进行授权规则设计
        //获取请求参数(http://localhost:8090/consumer/doFindById?id=20&origin=app1)
       String originValue=httpServletRequest.getParameter("origin");
       return originValue; //这个值返回给谁了?(方法的调用方)

        //基于请求头中的数据进行授权规则设计
        //基于请求头中origin的值，进行授权控制，http://localhost:8090/consumer/doFindById?id=20
        //请求头中origin的值设计可以在postman中进行实现。
        //String originValue=httpServletRequest.getHeader("origin");
        //return originValue;
    }
}
