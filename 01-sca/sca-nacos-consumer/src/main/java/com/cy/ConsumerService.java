package com.cy;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {
     /**
     * SentinelResource 注解描述的方法，
     * 可以通过指定链路进行流量统计并执行限流操作
     * @return
     */
     @SentinelResource("doGetResource")
     public String doGetResource(){
         //...access database
         System.out.println("do get resource");
         return "do get resource";
     }
}
