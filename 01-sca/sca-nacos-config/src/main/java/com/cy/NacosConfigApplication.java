package com.cy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class NacosConfigApplication {
    //org.slf4j.Logger
    //org.slf4j.LoggerFactory
    private static Logger logger= LoggerFactory.getLogger(NacosConfigApplication.class);
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigApplication.class,args);
       // logger.debug("===debug===");//debug<info<error,springboot默认日志级别为infoo
       // logger.info("===info===");
       // logger.error("===error===");
    }
    //@RefreshScope是spring中@Scope的升级版(特殊的@Scope)
    //@RefreshScope描述Controller类时，假如配置中新的配置发生变化，
    // 我们在对此类中的url资源进行访问时，会重新构建此对象。
    @RefreshScope//当配置信息发生变化时系统会基于@Value注解读取的值对属性进行重新初始化
    @RestController // @RestController是一个升级版的@Controller
    public class NacosConfigController{//此对象只创建一次

        public NacosConfigController(){
            System.out.println("NacosConfigController()");
        }

        @Value("${logging.level.com.cy:error}")//这里的error表示没有取到日志级别时给一个默认的级别
        private String logLevel;

        @Value("${server.tomcat.threads.max:200}")
        private Integer serverThreadMax;

        @Value("${page.pageSize:5}")
        private Integer pageSize;

        @GetMapping("/config/doGetPageSize")
        public String doGetPageSize(){
            //return String.format()
            return "page size is "+pageSize;
        }
        @GetMapping("/config/doGetServerThreadMax")
        public String doGetserverThreadMax(){
            return "server.threads.max is  "+serverThreadMax;
        }




        //http://localhost:8080/doGetLogLevel
        @GetMapping("/config/doGetLogLevel")
        public String doPrintLogLevel(){
            System.out.println("log.level="+logLevel);
            //通过如下日志输出检测，在配置中心改了日志级别是否会影响下面日志的输出
            logger.info("==level.info==");
            logger.debug("==level.debug==");
            logger.error("===level.error==");
            return "log level is "+logLevel;
            //假如希望在这里看到日志级别的变化需要使用@RefreshScope注解对类进行描述
        }
    }
}

