package com.cy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**服务提供方的一段业务demo代码*/
@RestController
@RequestMapping("/provider/template/")
public class RestProviderController {
    @Value("${server.port}")
    private String server;

    /**
     * 基于id执行删除操作
     * url=http://ip:port/provider/template/10
     *
     * @param id
     */
    @DeleteMapping("{id}")
    public void doDeleteById(@PathVariable Integer id){
        System.out.println(id+" is deleted by "+server);
    }

    @PostMapping
    public Map<String,Object> doPost(@RequestBody Map<String,Object> map){
        System.out.println("consumer post data: "+map);
        map.put("status", 1);
        map.put("server.port", server);
        return map;
    }

    @PutMapping
    public void doPut(@RequestBody Map<String,Object> map){
        System.out.println("put request map->"+map);
    }

}
