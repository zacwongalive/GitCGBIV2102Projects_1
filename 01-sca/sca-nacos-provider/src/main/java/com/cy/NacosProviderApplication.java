package com.cy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@SpringBootApplication
public class NacosProviderApplication {//启动此服务时，是启动了一个独立的进程吗？

    /**
     * 这个服务启动时，会自动向nacos发出一个http请求(发送请求的代码在
     * spring-cloud-starter-alibaba-nacos-discovery这个依赖中)，然后
     * 将这个服务注册到nacos服务端，nacos服务端会通过一个map来存储这个
     * 服务信息
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(NacosProviderApplication.class,args);
    }

    @Value("${server.port}")
    private String server;
    @RestController
    public class  ProviderController{
         @GetMapping(value = "/provider/echo/{string}")
         public String doEcho(@PathVariable String string, HttpServletRequest request){

             //获取请求中的数据
//             Enumeration<String> headerNames = request.getHeaderNames();
//             System.out.println(headerNames);
//             while(headerNames.hasMoreElements()){
//                 String headerName=headerNames.nextElement();
//                 System.out.println(headerName+"/"+request.getHeader(headerName));
//             }
             return server+" say:Hello Nacos Discovery " + string;
         }
    }
}
