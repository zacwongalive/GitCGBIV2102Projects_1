package com.cy.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class StringRedisTemplateTests {
    /**对于此对象一般应用操作redis中的字符串类型数据，此对象继承RedisTemplate.*/
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void testRedisStringOper()throws Exception{
        //获取用于操作字符串的值对象
       ValueOperations<String, String> valueOperations
                = stringRedisTemplate.opsForValue();
       //向redis中存储数据
       valueOperations.set("ip", "192.168.174.130");
       valueOperations.set("state","1",1, TimeUnit.SECONDS);
       valueOperations.decrement("state");
       // Thread.sleep(2000);
       //从redis中取数据
       String ip=valueOperations.get("ip");
       System.out.println("ip="+ip);
       String state=valueOperations.get("state");
       System.out.println("state="+state);
    }
}
