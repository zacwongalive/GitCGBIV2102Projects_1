package com.cy.redis;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.*;

import java.util.Random;
import java.util.UUID;

@SpringBootTest
public class JedisTests {

      @Test
      void testRedisStringOper(){
          Jedis jedis=new Jedis("192.168.174.130", 6380);
         // jedis.auth("123456");//假如你的redis设置了密码
          jedis.set("id", "101");
          jedis.set("name", "tony");
          System.out.println("set ok");
          String id=jedis.get("id");
          String name=jedis.get("name");
          System.out.println("id="+id+";name="+name);
          jedis.incr("id");
          jedis.incrBy("id", 2);
          System.out.println(jedis.strlen("name"));
          //......
      }
      @Test
      void testJedisPool()throws Exception{
          //1.构建连接池配置对象,并进行初始配置
          JedisPoolConfig config=new JedisPoolConfig();
          config.setMaxTotal(100);//设置最大连接数
          config.setMaxIdle(60000);//设置连接最大空闲时间
          //....
          //2.构建连接池(JedisPool)
          JedisPool jedisPool=new JedisPool(config,
                  "192.168.174.130", 6379);
          //3.从池中获取连接
          Jedis jedis=jedisPool.getResource();
         //4.通过jedis读写redis数据
          jedis.set("token", UUID.randomUUID().toString());//UUID用于获取一个随机字符串
          jedis.expire("token", 1);//设置key的有效时长(经常用于用户登录)
          Thread.sleep(2000);
          String token=jedis.get("token");
          System.out.println("token="+token);
          //5.释放资源
          jedisPool.close();
      }

      @Test
      void TestJedisCluster(){
          HostAndPort node=
                  new HostAndPort("192.168.174.130", 6379);
          JedisCluster jedisCluster=new JedisCluster(node);
          jedisCluster.set("E", "200");
          System.out.println("set OK");
      }


}
