package com.cy.redis;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class RedisShardTests {
    /**redis数据分片测试*/
    @Test
    void testRedisShard(){
        //1.构建jedis分片信息对象
        JedisShardInfo info1=//每个对象代表了对一个redis数据库的操作
        new JedisShardInfo("192.168.174.130", 6379);
        JedisShardInfo info2=
        new JedisShardInfo("192.168.174.130", 6380);
        JedisShardInfo info3=
        new JedisShardInfo("192.168.174.130", 6381);
        List<JedisShardInfo> list=new ArrayList<>();
        list.add(info1);
        list.add(info2);
        list.add(info3);
        //2.构建连接池配置对象
        JedisPoolConfig config=new JedisPoolConfig();
        config.setMaxTotal(100);
        //3.构建连接池对象(ShardedJedisPool)
        ShardedJedisPool pool=new ShardedJedisPool(config, list);
        ShardedJedis resource = pool.getResource();
        for(int i=0;i<10;i++) {
            //key/value是如何存储到不同的redis数据库的呢？(一致性hash算法)
            //一致性hash算法(保证相同内容hash求的值也是相同的)
            //存储过程中是基于key进行hash求值，然后再将hash值对服务器个数进行求余
            //确定了key要存储到的服务以后，再进行数据的存储
            resource.set("key-"+i,"value-"+i);
        }
        System.out.println("redis set ok");
        //取值时也会对key进行散列求值，基于这个值确定key所在redis数据库
        //我们不需要关心，这个key/value究竟存储到了哪个服务器
        System.out.println(resource.get("key-1"));
        System.out.println(resource.get("key-2"));
        //4.释放资源
        pool.close();
    }
}
