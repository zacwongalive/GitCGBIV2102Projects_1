package com.cy.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class RedisTemplateTests {
    /**此对应一般应用于操作redis中复杂数据结构数据*/
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testValueOper(){
        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.set("city", "beijing");//序列化存储
        Object city=valueOperations.get("city");
        System.out.println(city);
    }

    /**刷库(清缓存)*/
    @Test
    void testClear(){
         redisTemplate.execute(new RedisCallback() {
             @Override
             public Object doInRedis(RedisConnection connection) throws DataAccessException {
                 //connection.flushAll();
                 connection.flushDb();
                 return null;
             }
         });
    }

    /**set结构数据的操作*/
    @Test
    void testSetOper(){
        SetOperations setOperations = redisTemplate.opsForSet();
        //向key为set1的集合中添加新的数据
        setOperations.add("set1", "A","B","C");
        //对于set集合而言不允许有重复数据
        setOperations.add("set1", "A","D","C");
        //获取set集合元素个数
        Long set11 = setOperations.size("set1");
        System.out.println("set1.size="+set11);
        //获取指定set集合成员
        Set set1 = setOperations.members("set1");
        System.out.println(set1);
        setOperations.add("set2", "E","F");
        //合并集合
        Set union = setOperations.union("set1", "set2");
        System.out.println(union);
    }
    /**操作list结构数据*/
    @Test
    void testListOper(){
        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPush("A", 100);
        listOperations.leftPush("A", 200);
        listOperations.leftPush("A", 300);
        System.out.println(listOperations.range("A",0,-1));
       // listOperations.remove("A", 1, 100);
        Object o1=listOperations.rightPop("A");
        Object o2=listOperations.rightPop("A");
        Object o3=listOperations.rightPop("A");
        System.out.println(o1+","+o2+","+o3);
        Object o4=listOperations.rightPop("A");
        System.out.println(o4);
        //阻塞式方式(没有时则等待)
        Object o5=listOperations.rightPop("A", 10, TimeUnit.SECONDS);
        System.out.println(o5);

    }

    /**hash 数据结构*/
    @Test
    void testHashOper(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put("member", "id", 101);
        hashOperations.put("member", "username", "tony");
        hashOperations.put("member", "username", "jason");
        Map<String,Object> map=new HashMap<>();
        map.put("mobile", "177777");
        map.put("email", "tony@t.com");
        hashOperations.putAll("member", map);
        //putIfAbsent应用时hashKey不存在则直接添加信息，返回true，假如存在则什么也不做
        hashOperations.putIfAbsent("member", "gender", "male");
        hashOperations.putIfAbsent("member", "gender", "female");
        Object id = hashOperations.get("member", "id");
        Object username=hashOperations.get("member","username");
        System.out.println("id="+id);
        System.out.println("username="+username);
        Object entries=hashOperations.entries("member");
        System.out.println("entries="+entries);

    }


}
